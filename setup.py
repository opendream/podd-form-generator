from setuptools import setup, find_packages
import sys, os

version = '0.1.0'

# os.system('pip install git+https://github.com/pphetra/pyth.git@master')

setup(name='podd_form_generator',
      version=version,
      description="Create report definition from omnigrafflee diagram",
      long_description="""\
""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='podd',
      author='Polawat Phetra',
      author_email='pphetra@gmail.com',
      url='https://bitbucket.org/opendream/podd-form-generator',
      packages=['podd_form_generator'],
      zip_safe=True,
      install_requires=[
          'Shapely==1.4.4',
          #'pyth==0.6.1',
      ],
      dependency_links=[
          'git+https://github.com/pphetra/pyth.git#egg=pyth-0.6.1'
      ],
      )
