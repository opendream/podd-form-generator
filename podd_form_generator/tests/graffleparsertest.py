import unittest
import shapely
from podd_form_generator.formgenerator import dump
from podd_form_generator.graffleparser import *
import plistlib

import os
curdir = os.path.dirname(__file__)

class TestGraffleParser(unittest.TestCase):

    def test_parse_bounds(self):
        box = parse_bounds('{{745, 472}, {224, 405}}')
        self.assertIsInstance(box, shapely.geometry.polygon.Polygon)

    def test_parse_page(self):
        root = plistlib.readPlist(os.path.join(curdir, 'testpage.plist'))
        page = parse_page(root['page'])
        self.assertIsInstance(page, Page)
        self.assertEqual(root['page']['ID'], page.get_id())

    def test_parse_text_question(self):
        root = plistlib.readPlist(os.path.join(curdir, 'testTextQuestion.plist'))['question']
        question = parse_question(root)
        self.assertIsInstance(question, Question)
        self.assertEqual(root['ID'], question.get_id())
        self.assertEqual('what is your name?', question.get_title())
        self.assertEqual('name', question.get_name())
        self.assertTrue(question.is_required())
        self.assertEqual('This field is Required', question.get_required_msg())
        self.assertEqual('text', question.get_type())

    def test_parse_integer_question(self):
        root = plistlib.readPlist(os.path.join(curdir, 'testIntegerQuestion.plist'))['question']
        question = parse_question(root)
        self.assertIsInstance(question, Question)
        self.assertEqual(root['ID'], question.get_id())
        self.assertEqual('how old are you?', question.get_title())
        self.assertEqual('age', question.get_name())
        self.assertTrue(question.is_required())
        self.assertEqual('Age is required', question.get_required_msg())
        self.assertEqual('integer', question.get_type())

    def test_parse_multiple_question(self):
        root = plistlib.readPlist(os.path.join(curdir, 'testMultipleQuestion.plist'))['question']
        question = parse_multiple_question(root)
        self.assertIsInstance(question, Question)
        self.assertEqual(root['ID'], question.get_id())
        self.assertEqual('title', question.get_title())
        self.assertEqual('Multiple Select', question.get_name())
        self.assertTrue(question.is_required())
        self.assertEqual('This field is Required', question.get_required_msg())
        self.assertEqual('multiple', question.get_type())
        self.assertEqual(8, len(question.get_answers()))

    def test_parse_multiple_single_question(self):
        root = plistlib.readPlist(os.path.join(curdir, 'testSingleSelectQuestion.plist'))['question']
        question = parse_multiple_question(root)
        self.assertIsInstance(question, Question)
        self.assertEqual(root['ID'], question.get_id())
        self.assertEqual('What kind of your pet', question.get_title())
        self.assertEqual('petType', question.get_name())
        self.assertTrue(question.is_required())
        self.assertEqual('This field is Required', question.get_required_msg())
        self.assertEqual('single', question.get_type())
        self.assertEqual(5, len(question.get_answers()))
        self.assertEqual('Other', question.get_other())

    def test_extract_user_info_graphics(self):
        root = plistlib.readPlist(os.path.join(curdir, 'testgs.plist'))
        self.assertTrue(len(root['GraphicsList']) == 2)
        gs = extract_user_info_graphics(root)
        self.assertTrue(len(gs) == 1)

    def test_parse_transition(self):
        root = plistlib.readPlist(os.path.join(curdir, 'testTransition.plist'))
        ts = parse_transitions(root)
        self.assertEqual(1, len(ts))
        self.assertEqual('true', ts[0].get_expression())
        self.assertEqual(147, ts[0].get_from_page_id())
        self.assertEqual(225, ts[0].get_to_page_id())

    def test_parse_form(self):
        root = plistlib.readPlist(os.path.join(curdir, 'testParseForm.plist'))
        form = parse(root)
        self.assertEqual(3, len(form.get_questions()))
        self.assertEqual(2, len(form.get_transitions()))
        self.assertEqual(3, len(form.get_pages()))

        for p in form.get_pages():
            self.assertEqual(1, len(p.get_questions()))

        dump(form)


if __name__ == '__main__':
    unittest.main()