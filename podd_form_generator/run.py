from podd_form_generator.graffleparser import parse
from podd_form_generator.formgenerator import dump
import plistlib
import sys


def runme():
    root = plistlib.readPlist(sys.argv[1])
    print dump(parse(root))


if "__main__" == __name__:
    runme()