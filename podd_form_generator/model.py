class Element():
    def __init__(self, item_id):
        self._id = item_id

    def get_id(self):
        return self._id


class Question(Element):
    def __init__(self, item_id, bound, name, title, answer_type, required=False, require_msg=None, ordered=0, hidden_name=None, validations=[]):
        Element.__init__(self, item_id)
        self._bound = bound
        self._name = name
        self._title = title
        self._type = answer_type
        self._required = required
        self._required_msg = require_msg
        self._ordered = ordered
        self._hidden_name = hidden_name
        self._validations = validations

    def get_name(self):
        return self._name

    def get_title(self):
        return self._title

    def get_type(self):
        return self._type

    def get_required_msg(self):
        return self._required_msg

    def is_required(self):
        return self._required

    def get_bound(self):
        return self._bound

    def get_ordered(self):
        return self._ordered

    def get_hidden_name(self):
        return self._hidden_name

    def get_validations(self):
        return self._validations


class MultipleQuestion(Question):
    def __init__(self, item_id, bound, name, title, answer_type, required=False, require_msg=None, answers=None, other=None, validations=[]):
        Question.__init__(self, item_id, bound, name, title, answer_type, required, require_msg, validations)
        self._answers = answers
        self._other = other

    def get_answers(self):
        return self._answers

    def get_other(self):
        return self._other

class DataQuestion(Question):
    def __init__(self, item_id, bound, name, title, answer_type, required=False, require_msg=None, dataUrl=None, filterFields=None):
        Question.__init__(self, item_id, bound, name, title, answer_type, required, require_msg)
        self._dataUrl = dataUrl
        self._filterFields = filterFields

    def get_dataUrl(self):
        return self._dataUrl

    def get_filterFields(self):
        return self._filterFields



class Transition(Element):
    def __init__(self, item_id, from_page_id, to_page_id, expression=None, order=None):
        Element.__init__(self, item_id)
        self._from_page_id = from_page_id
        self._to_page_id = to_page_id
        self._expression = expression
        if order:
            self._order = order
        else:
            self._order = 0

    def set_expression(self, e):
        self._expression = e

    def get_expression(self):
        return self._expression

    def get_from_page_id(self):
        return self._from_page_id

    def get_to_page_id(self):
        return self._to_page_id

    def get_order(self):
        return self._order


class Confirm_Dialog():
    def __init__(self, message, condition, bound):
        self._message = message
        self._condition = condition
        self._bound = bound

    def get_condition(self):
        return self._condition

    def get_message(self):
        return self._message

    def get_bound(self):
        return self._bound


class Page(Element):
    def __init__(self, item_id, bound):
        Element.__init__(self, item_id)
        self._bound = bound
        self._questions = []
        self._confirm_dialog = None

    def contain_bound(self, elm):
        return self._bound.contains(elm.get_bound())

    def add_question(self, question):
        self._questions.append(question)

    def get_questions(self):
        return sorted(self._questions, key=lambda q: q.get_ordered())

    def set_confirm_dialog(self, dialog):
        self._confirm_dialog = dialog

    def get_confirm_dialog(self):
        return self._confirm_dialog


class Form():
    def __init__(self, pages=None, questions=None, transitions=None, confirm_dialogs=None, force_location=False):
        if pages:
            self._pages = pages
        else:
            self._pages = []

        if transitions:
            self._transitions = transitions
        else:
            self._transitions = []

        if questions:
            self._questions = questions
        else:
            self._questions = []

        self._confirm_dialogs = confirm_dialogs

        self.assign_question_to_page()
        self.assign_confirm_dialog_to_page()
        self._start_page_id = self.find_start_page()
        self.force_location = force_location

    def add_page(self, page):
        self._pages.append(page)

    def add_question(self, question):
        self._questions.append(question)

    def add_transition(self, transition):
        self._transitions.append(transition)

    def get_pages(self):
        return self._pages

    def get_questions(self):
        return self._questions

    def get_transitions(self):
        return self._transitions

    def get_start_page_id(self):
        return self._start_page_id

    def assign_question_to_page(self):
        for q in self._questions:
            for p in self._pages:
                if p.contain_bound(q):
                    p.add_question(q)

    def assign_confirm_dialog_to_page(self):
        if self._confirm_dialogs:
            for c in self._confirm_dialogs:
                for p in self._pages:
                    if p.contain_bound(c):
                        p.set_confirm_dialog(c)

    def find_start_page(self):
        pids = [p.get_id() for p in self.get_pages()]
        if pids:
            for t in self.get_transitions():
                assert isinstance(t, Transition)
                pid = t.get_to_page_id()
                if pid in pids:
                    pids.remove(pid)
            return pids[0]
        else:
            return None
