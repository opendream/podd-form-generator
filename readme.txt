#usage:
# -*- coding: utf-8 -*-

from podd_form_generator.graffleparser import parse
from podd_form_generator.formgenerator import dump
import plistlib

root = plistlib.readPlist('/Users/pphetra/Desktop/test.graffle/data.plist')
report_definition_json = dump(parse(root))

import sys, codecs
class EncodedOut:
    def __init__(self, enc):
        self.enc = enc
        self.stdout = sys.stdout
    def __enter__(self):
        if sys.stdout.encoding is None:
            w = codecs.getwriter(self.enc)
            sys.stdout = w(sys.stdout)
    def __exit__(self, exc_ty, exc_val, tb):
        sys.stdout = self.stdout


with EncodedOut('utf-8'):
  print report_definition_json
