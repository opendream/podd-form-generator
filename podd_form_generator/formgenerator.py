from podd_form_generator.model import Form, Question, MultipleQuestion, Page, Transition, DataQuestion
import json
import re

__author__ = 'pphetra'


def dump_questions(form, root):
    root['questions'] = []
    hidden_pattern = re.compile('([^\]]+)\[(.+)\]')

    for q in form.get_questions():
        assert isinstance(q, Question)
        name = q.get_name()
        hidden_name = None
        m = hidden_pattern.match(name)
        if m:
            name = m.group(1)
            hidden_name = m.group(2)

        obj = {
            'id': q.get_id(),
            'name': name,
            'title': q.get_title(),
            'type': q.get_type(),
            'validations': []
        }
        if hidden_name:
            obj['hiddenName'] = hidden_name

        if q.is_required():
            obj['validations'].append(
                {
                    'type': 'require',
                    'message': q.get_required_msg()
                }
            )

        for v in q.get_validations():
            obj['validations'].append(v)

        if isinstance(q, DataQuestion):
            assert isinstance(q, DataQuestion)
            obj['dataUrl'] = q.get_dataUrl()
            obj['filterFields'] = q.get_filterFields()

        if isinstance(q, MultipleQuestion):
            assert isinstance(q, MultipleQuestion)
            obj['items'] = []
            for answer in q.get_answers():
                text = answer
                hidden_value = None
                m = hidden_pattern.match(text)
                if m:
                    text = m.group(1)
                    hidden_value = m.group(2)

                item = {
                    'id': answer,
                    'text': text
                }
                if hidden_value:
                    item['hiddenValue'] = hidden_value

                obj['items'].append(item)

            if q.get_other():
                obj['freeTextChoiceEnable'] = True
                obj['freeTextId'] = q.get_other()
                obj['freeTextText'] = q.get_other()
                obj['freeTextName'] = q.get_name() + 'Other'

        root['questions'].append(obj)


def dump_confirm_dialog(dialog):
    return {
        'message': dialog.get_message(),
        'condition': dialog.get_condition()
    }


def dump_pages(form, root):
    root['pages'] = []
    for p in form.get_pages():
        assert isinstance(p, Page)
        obj = {
            'id': p.get_id(),
            'questions': [q.get_id() for q in p.get_questions()]
        }
        if p.get_confirm_dialog():
            obj['confirmDialog'] = dump_confirm_dialog(p.get_confirm_dialog())
        root['pages'].append(obj)


def dump_transitions(form, root):
    root['transitions'] = []
    for t in form.get_transitions():
        assert isinstance(t, Transition)
        obj = {
            "from": t.get_from_page_id(),
            "to": t.get_to_page_id(),
            "expression": t.get_expression(),
            "order": t.get_order()
        }
        root['transitions'].append(obj)
    root['transitions'] = sorted(root['transitions'], key=lambda k: "%06d%02d" % (k['from'], k['order']))



def dump(form):
    assert isinstance(form, Form)
    root = {
        "startPageId": form.get_start_page_id(),
    }
    if form.force_location:
        root['forceLocation'] = True
    dump_questions(form, root)
    dump_pages(form, root)
    dump_transitions(form, root)
    return json.dumps(root, sort_keys=True, indent=4, separators=(',', ': '), ensure_ascii=False, encoding='utf8')
